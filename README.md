# Booster Task

## Purpose

Writing reactive code with project reactor is not trivial for several reasons:
1. Error handling is hidden and can be easily forgotten.
   1. Error handling in ```Publisher``` is automatic and once an error occurs, subsequent code is skipped.
   2. Even seasoned developers can easily miss a place where exception can be thrown.
2. A lot of boilerplate code required.
   1. Retries and circuit breakers in various places require repetitive code.
   2. Monitoring retries, circuit breakers, thread pools with [micrometer](https://micrometer.io/).
   3. Using ```Mono.zip``` to aggregate a set of tasks executed concurrently.
3. When to use threads and how to use threads correctly is often a non-trivial task for beginners.

Booster task tries to address these problems by wrapping
1. concurrent execution, 
2. retries and circuit breakers,
3. metrics monitoring,
4. error handling and error recovery
all in a single library.

## Getting started

### Project Setup

#### Maven

pom.xml
```xml
<dependency>
  <groupId>io.gitlab.booster</groupId>
  <artifactId>booster-task</artifactId>
  <version>VERSION_TO_USE</version>
</dependency>
```
if you haven't yet set up your repository:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/61046646/-/packages/maven</url>
  </repository>
</repositories>
```

#### Gradle Groovy

```groovy
implementation 'io.gitlab.booster:booster-task:VERSION_TO_USE'
```
setup maven repository:
```groovy
maven {
  url 'https://gitlab.com/api/v4/groups/61046646/-/packages/maven'
}
```

#### Gradle Kotlin

```kotlin
implementation("io.gitlab.booster:booster-task:VERSION_TO_USE")
```
setup maven repository:
```kotlin
maven("https://gitlab.com/api/v4/groups/61046646/-/packages/maven")
```

## How to Use

### Tasks

To learn how to use booster-task, one needs to first understand what is a task and what 
are the different kinds of tasks supported in booster-task library.

To put it short, one can view a task as a function, one that takes an input, and generates 
an output.

There are in essence, 3 types of tasks:
1. Simple task: a task that takes an input and produces an output, without any dependency. Tasks such as calling to Redis, HTTP server are these type of tasks.
2. Sequential task: two tasks that need to be executed in sequence, such as calling out to Redis first to fetch data, then call to database.
3. Parallel task: an input divided into multiple segments, with each segment processed concurrently and the final result from each segment merged into one.

A parallel task can be further divided into two categories:
1. A task that takes a list of homogeneous elements, and process each element concurrently, then merge the result into one list, or 
2. A task that takes a tuple of different elements, each element processed concurrently, then final result merged into another tuple.

Accordingly, booster-task has the following classes:
1. Simple tasks: ```AsyncTask```, ```SynchronousTask```
2. Sequential tasks: ```SequentialTask```
3. Parallel tasks:
   1. Homogeneous parallel task: ```ParallelTask```
   2. Tuple parallel tasks: ```Tuple2Task```, ```Tuple3Task```, ```Tuple4Task```, ```Tuple5Task```, ```Tuple6Task```, ```Tuple7Task```, ```Tuple8Task```

#### The Anatomy of a Task 

##### Task Input 

A task input can be the actual request object, or an [Either](https://arrow-kt.io/docs/apidocs/arrow-core/arrow.core/-either/) 
of ```Throwable``` as left value, and actual request object as the right value, or a [Mono](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html) of Either. 

The actual request can be null, and in case of either or mono of either, either can contain a left value of throwable.

##### Input Exception Handler 

All tasks have an exception handler used in case when the input is a left value and contains instead of the 
normal request object, an exception. When created with exception handler omitted or set to null, a default 
exception handler is assigned to the task, and in case the input is exception, the embedded exception 
will be thrown and the output of the task becomes a mono of either with left value of the input exception.

##### Task Output

All task produces a [Mono](https://projectreactor.io/docs/core/release/api/reactor/core/publisher/Mono.html) of [Either](https://arrow-kt.io/docs/apidocs/arrow-core/arrow.core/-either/) with left value of ```Throwable``` and right value of actual output type.

##### Error Handling 

All tasks eventually produce an either with a possible left value. Internally, all 
tasks will produce a Mono with a possible error value. In case of an error, this will 
be converted into an Either Left, and exception will be logged and recorded in metrics.

#### Simple Tasks 
Simple tasks are created with:
1. Optional ```ExecutorService```
2. Optional [Resilience4j Retry](https://resilience4j.readme.io/docs/retry)
3. Optional [Resilience4j Circuit Breaker](https://resilience4j.readme.io/docs/circuitbreaker)
4. A processor that handles the input request object.

If an ```ExecutorService``` is not provided, the task will be executed on the calling thread.
If an ```ExecutorService``` is provided, the ```ExecutorService``` will be monitored through 
micrometer and metrics will be reported.

Optional retry and circuit breaker can be provided when creating the task. If provided, retry and 
circuit breaking will be applied to the task. Only simple tasks are allowed to have retries or 
circuit breakers. This is not applied only to HTTP clients, such that other tasks that can 
potentially fail or have long latencies can take benefit of retries and circuit breakers, such 
as calls to Redis etc.

Simple tasks are separated into two classes:
1. AsyncTask, and 
2. SynchronousTask.

The difference is ```AsyncTask``` has a processor that produces a Mono of response object, with a 
possible error in the Mono. ```SynchronousTask``` has a processor that either produces 
a response object or throws an exception. ```SynchronousTask``` is created with a MonoSink 
to wrap the synchronous execution in an asynchronous manner.

#### Sequential Task 

Sequential tasks chain two tasks together, the output of the first task becomes the 
input of the second task. Sequential tasks don't have thread pools, or retries or 
circuit breakers.

#### Parallel Tasks 

Both types of parallel tasks have one or more embedded task, equal to the number of types 
of input elements. Each embedded task is responsible for processing one type of element and 
create response object for that element.

Both types of parallel tasks have an aggregator that aggregates the output of the embedded 
tasks together and convert that to final task response.

##### Homogeneous Parallel Task 

A Homogeneous parallel task takes a list of same elements, processes them concurrently then 
merge the result of each element into a list as response.

##### Tuple Task

A tuple task takes an input of a tuple, an either of a tuple, or a mono of an either of 
a tuple, and produces a mono of an either of tuple.

There are 7 tuple tasks, corresponding to 2 to 7 different elements in the tuple.

### Usage 

There are two ways to create a task:
1. Use task constructor, or 
2. Use Kotlin DSL task builder.

For details, check unit tests for each type of task.
